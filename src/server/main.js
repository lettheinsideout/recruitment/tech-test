const express = require(`express`);
const middleware = require(`./lib/middleware`);
const routes = require(`./lib/routes`);
const config = require(`./lib/config`);
const log = require(`./lib/logger`);

const app = express();
app.disable(`x-powered-by`);

app.use(routes);

app.use(middleware.notFoundHandler);
app.use(middleware.errorHandler);

app.listen(config.PORT, () => {
	log.info(`Server listening on port "${config.PORT}"`);
});
