const mockDb = require(`./__mocks__/db`);

async function query(statement, values) {
	const result = await mockDb.query(statement, values);
	return result.data;
}

async function similateDelay(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

async function getUserStats() {
	await similateDelay(1000);
	return {
		numTotal: 23489,
		numActive30Days: 4821,
	};
}

async function getTherapistStats() {
	await similateDelay(1000);
	return {
		numTotal: 517,
		numActive30Days: 492,
	};
}

async function getEmployerStats() {
	await similateDelay(1000);
	return {
		numTotal: 64,
		numActive30Days: 58,
	};
}

async function getCallStats() {
	await similateDelay(1000);
	return {
		numTotal: 582992,
	};
}

async function getRevenueStats() {
	await similateDelay(1000);
	return {
		mrr: 239891,
		arr: 239891 * 12,
	};
}

async function getFeedbackStats() {
	await similateDelay(1000);
	return {
		totalRatings: 571,
		avgRating: 4.7,
	};
}

async function getAppointmentById(id) {
	const rows = await db.query(`SELECT * FROM appointments`);
	return rows.find(row => row.appointment_id === id);
}

module.exports = {
	query,
	getUserStats,
	getTherapistStats,
	getEmployerStats,
	getCallStats,
	getRevenueStats,
	getFeedbackStats,
	getAppointmentById,
};
