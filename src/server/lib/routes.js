const express = require(`express`);
const { asyncMiddleware } = require(`middleware-async`);
const db = require(`./db`);

const router = express.Router();

// TASK 1
router.get(
	`/appointments`,
	asyncMiddleware(async (req, res, next) => {
		const rows = await db.query(`SELECT * FROM appointments WHERE user_id=${req.query.user_id}`);
		res.json({ success: true, rows });
	}),
);

// TASK 2
router.get(
	`/stats`,
	asyncMiddleware(async (req, res, next) => {
		const userStats = await db.getUserStats();
		const therapistStats = await db.getTherapistStats();
		const employerStats = await db.getEmployerStats();
		const callStats = await db.getCallStats();
		const revenueStats = await db.getRevenueStats();
		const feedbackStats = await db.getFeedbackStats();

		res.json({
			success: true,
			data: {
				numUsers: userStats.numTotal,
				numUsersActive: userStats.numActive30Days,
				numTherapists: therapistStats.numTotal,
				numTherapistsActive: therapistStats.numActive30Days,
				numEmployers: employerStats.numTotal,
				numEmployersActive: employerStats.numActive30Days,
				numCalls: callStats.numTotal,
				monthlyRevenue: revenueStats.mrr,
				annualRevenue: revenueStats.arr,
				totalRatings: feedbackStats.totalRatings,
				avgRating: feedbackStats.avgRating,
			},
		});
	}),
);

// TASK 3
router.get(
	`/currency`,
	asyncMiddleware(async (req, res, next) => {
		// TODO: SOLUTION
	}),
);

// TASK 4
router.post(
	`/billing`,
	asyncMiddleware(async (req, res, next) => {
		const appointments = await db.query(`SELECT * FROM appointments WHERE invoiced = FALSE AND completed = TRUE`);

		appointments.forEach(async appointment => {
			await db.query(`INSERT INTO billing_items (employer_id, price) VALUES ($1, $2)`, [
				appointment.employer_id,
				appointment.price,
			]);
			await db.query(`UPDATE appointments SET invoiced = TRUE WHERE appointment_id = $1`, [appointment.appointment_id]);
		});

		res.json({ success: true, appointments: appointments.length });
	}),
);

// TASK 5
router.get(
	`/billing`,
	asyncMiddleware(async (req, res, next) => {
		const appointments = await db.query(`SELECT * FROM appointments`);
		// TODO: SOLUTION
		res.json({ success: true });
	}),
);

module.exports = router;
