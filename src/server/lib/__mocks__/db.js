const MOCK_DATA = {
	appointments: [
		{
			appointment_id: 1,
			user_id: 1,
			therapist_id: 1,
			employer_id: 1,
			date_appointment: `2021-04-10T10:30:00`,
			date_user_join: `2021-04-10T10:29:00`,
			date_therapist_join: `2021-04-10T10:30:00`,
			cancelled_by: null,
			date_cancelled: null,
		},
		{
			appointment_id: 2,
			user_id: 1,
			therapist_id: 1,
			employer_id: 1,
			date_appointment: `2021-04-17T10:30:00`,
			date_user_join: `2021-04-17T10:28:00`,
			date_therapist_join: null,
			cancelled_by: null,
			date_cancelled: null,
		},
		{
			appointment_id: 3,
			user_id: 1,
			therapist_id: 1,
			employer_id: 1,
			date_appointment: `2021-04-24T10:30:00`,
			date_user_join: null,
			date_therapist_join: `2021-04-24T10:31:00`,
			cancelled_by: null,
			date_cancelled: null,
		},
		{
			appointment_id: 4,
			user_id: 2,
			therapist_id: 1,
			employer_id: 1,
			date_appointment: `2021-04-16T15:00:00`,
			date_user_join: null,
			date_therapist_join: null,
			cancelled_by: null,
			date_cancelled: null,
		},
		{
			appointment_id: 5,
			user_id: 2,
			therapist_id: 1,
			employer_id: 1,
			date_appointment: `2021-04-21T11:00:00`,
			date_user_join: null,
			date_therapist_join: null,
			cancelled_by: `USER`,
			date_cancelled: `2021-04-20T22:00:00`,
		},
		{
			appointment_id: 6,
			user_id: 2,
			therapist_id: 1,
			employer_id: 1,
			date_appointment: `2021-04-25T11:00:00`,
			date_user_join: null,
			date_therapist_join: null,
			cancelled_by: `USER`,
			date_cancelled: `2021-04-24T08:00:00`,
		},
		{
			appointment_id: 7,
			user_id: 2,
			therapist_id: 1,
			employer_id: 1,
			date_appointment: `2021-04-30T11:00:00`,
			date_user_join: null,
			date_therapist_join: null,
			cancelled_by: `THERAPIST`,
			date_cancelled: `2021-04-30T10:50:00`,
		},
	],
};

function getMockData(statement) {
	switch (true) {
		case statement.includes(`appointments`):
			return MOCK_DATA.appointments;
		default:
			return [];
	}
}

async function query(statement, values) {
	const mockData = getMockData(statement);

	return Promise.resolve({
		count: mockData.length,
		data: mockData,
	});
}

module.exports = {
	query,
};
