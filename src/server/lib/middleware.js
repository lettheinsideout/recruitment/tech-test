function notFoundHandler(req, res, next) {
	throw new Error(`Route '${req.path}' not found`);
}

function errorHandler(err, req, res, next) {
	res.status(404).json({
		success: false,
		error: err.message,
	});
}

module.exports = {
	notFoundHandler,
	errorHandler,
};
