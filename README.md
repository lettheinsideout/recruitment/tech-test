# Tech Test

A pair programming tech test.

## Quick Start

1. You'll need to use a HTTP API testing tool such as [Postman](https://www.postman.com) to work with the API.
2. Install the dependencies with `npm install`.
3. Start the HTTP server with `npm start`.
4. Read the task instructions provided by the interviewer.
